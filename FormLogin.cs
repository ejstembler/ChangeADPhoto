﻿using System;
using System.Windows.Forms;

namespace ChangeADPhoto
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        public string Username
        {
            get
            {
                return textBoxUsername.Text;
            }
            set
            {
                textBoxUsername.Text = value;
            }
        }

        public string Password
        {
            get
            {
                return textBoxPassword.Text;
            }
            set
            {
                textBoxPassword.Text = value;
            }
        }

        private void textBoxUsername_TextChanged(object sender, EventArgs e)
        {
            checkOKEnabled();
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            checkOKEnabled();
        }

        private void checkOKEnabled()
        {
            buttonOK.Enabled = textBoxUsername.Text != "" && textBoxPassword.Text != "";
        }
    }
}
