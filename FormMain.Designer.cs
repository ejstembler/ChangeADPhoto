﻿namespace ChangeADPhoto
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelPhotoFilename = new System.Windows.Forms.Label();
            this.textBoxPhotoFilename = new System.Windows.Forms.TextBox();
            this.buttonBrowsePhotos = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panelPhoto = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonSet = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panelPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPhotoFilename
            // 
            this.labelPhotoFilename.AutoSize = true;
            this.labelPhotoFilename.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelPhotoFilename.Location = new System.Drawing.Point(12, 9);
            this.labelPhotoFilename.Name = "labelPhotoFilename";
            this.labelPhotoFilename.Size = new System.Drawing.Size(83, 13);
            this.labelPhotoFilename.TabIndex = 0;
            this.labelPhotoFilename.Text = "Photo file name:";
            // 
            // textBoxPhotoFilename
            // 
            this.textBoxPhotoFilename.Location = new System.Drawing.Point(101, 6);
            this.textBoxPhotoFilename.Name = "textBoxPhotoFilename";
            this.textBoxPhotoFilename.Size = new System.Drawing.Size(171, 20);
            this.textBoxPhotoFilename.TabIndex = 1;
            this.textBoxPhotoFilename.TextChanged += new System.EventHandler(this.textBoxPhotoFilename_TextChanged);
            // 
            // buttonBrowsePhotos
            // 
            this.buttonBrowsePhotos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonBrowsePhotos.Location = new System.Drawing.Point(278, 4);
            this.buttonBrowsePhotos.Name = "buttonBrowsePhotos";
            this.buttonBrowsePhotos.Size = new System.Drawing.Size(23, 23);
            this.buttonBrowsePhotos.TabIndex = 2;
            this.buttonBrowsePhotos.Text = "…";
            this.toolTip.SetToolTip(this.buttonBrowsePhotos, "Browse… | Browse for photos…");
            this.buttonBrowsePhotos.UseVisualStyleBackColor = true;
            this.buttonBrowsePhotos.Click += new System.EventHandler(this.buttonBrowsePhotos_Click);
            // 
            // panelPhoto
            // 
            this.panelPhoto.AutoScroll = true;
            this.panelPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPhoto.Controls.Add(this.pictureBox);
            this.panelPhoto.Location = new System.Drawing.Point(101, 32);
            this.panelPhoto.Name = "panelPhoto";
            this.panelPhoto.Size = new System.Drawing.Size(200, 200);
            this.panelPhoto.TabIndex = 3;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(100, 50);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "JPEG images (*.jpg)|*.jpg|All files (*.*) | *.*";
            // 
            // buttonSet
            // 
            this.buttonSet.Enabled = false;
            this.buttonSet.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSet.Location = new System.Drawing.Point(226, 238);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(75, 23);
            this.buttonSet.TabIndex = 4;
            this.buttonSet.Text = "Set Photo";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClose.Location = new System.Drawing.Point(12, 238);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 5;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClear.Location = new System.Drawing.Point(145, 238);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 6;
            this.buttonClear.Text = "Clear Photo";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // FormMain
            // 
            this.AcceptButton = this.buttonSet;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(313, 271);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonSet);
            this.Controls.Add(this.panelPhoto);
            this.Controls.Add(this.buttonBrowsePhotos);
            this.Controls.Add(this.textBoxPhotoFilename);
            this.Controls.Add(this.labelPhotoFilename);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Active Directory Photo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panelPhoto.ResumeLayout(false);
            this.panelPhoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPhotoFilename;
        private System.Windows.Forms.TextBox textBoxPhotoFilename;
        private System.Windows.Forms.Button buttonBrowsePhotos;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Panel panelPhoto;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonClear;
    }
}

