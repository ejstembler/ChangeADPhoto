﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.DirectoryServices;

namespace ChangeADPhoto
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private static string domainController;
        private static string photoKey;

        private void FormMain_Load(object sender, EventArgs e)
        {
            domainController = ConfigurationManager.AppSettings["Domain Controller"];
            photoKey = ConfigurationManager.AppSettings["Photo Key"] ?? "thumbnailPhoto";
        }

        private void buttonBrowsePhotos_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                textBoxPhotoFilename.Text = openFileDialog.FileName;
                Cursor savedCursor = this.Cursor;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    pictureBox.Load(openFileDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, string.Format("Unable to display photo.\n\n{0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBoxPhotoFilename.Text = "";
                    pictureBox.Image = null;
                }
                finally
                {
                    this.Cursor = savedCursor;
                }
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxPhotoFilename_TextChanged(object sender, EventArgs e)
        {
            buttonSet.Enabled = File.Exists(textBoxPhotoFilename.Text);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            using (FormLogin formLogin = new FormLogin())
            {
                if (formLogin.ShowDialog(this) == DialogResult.OK)
                {
                    Cursor savedCursor = this.Cursor;
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        clearPhoto(domainController, formLogin.Username, formLogin.Password);
                        textBoxPhotoFilename.Text = "";
                        pictureBox.Image = null;
                        MessageBox.Show(this, "Your profile photo has been cleared.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, string.Format("Unable to clear photo.\n\n{0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        this.Cursor = savedCursor;
                    }
                }
            }
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            using (FormLogin formLogin = new FormLogin())
            {
                if (formLogin.ShowDialog(this) == DialogResult.OK)
                {
                    Cursor savedCursor = this.Cursor;
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        setPhoto(domainController, formLogin.Username, formLogin.Password, textBoxPhotoFilename.Text);
                        MessageBox.Show(this, "You profile photo has been updated.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, string.Format("Unable to set photo.\n\n{0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        this.Cursor = savedCursor;
                    }
                }
            }
        }

        private static void clearPhoto(string domainController, string username, string password)
        {
            using (DirectoryEntry user = getADUser(domainController, username, password))
            {
                // Clear existing picture
                user.Properties[photoKey].Clear();
                user.CommitChanges();
            }
        }

        private static void setPhoto(string domainController, string username, string password, string filename)
        {
            // Open file
            FileStream photoFile = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Retrive Data into a byte array variable
            byte[] binaryData = new byte[photoFile.Length];

            int bytesRead = photoFile.Read(binaryData, 0, (int)photoFile.Length);
            photoFile.Close();

            using (DirectoryEntry user = getADUser(domainController, username, password))
            {
                // Clear existing picture if exists
                user.Properties[photoKey].Clear();
                // Update attribute with binary data from file
                user.Properties[photoKey].Add(binaryData);
                user.CommitChanges();
            }
        }

        private static DirectoryEntry getADUser(string domainController, string username, string password)
        {
            // Connect to AD
            using (DirectoryEntry de = new DirectoryEntry(@"LDAP://" + domainController, username, password))
            {
                using (DirectorySearcher adSearch = new DirectorySearcher(de))
                {
                    adSearch.Filter = string.Format("(sAMAccountName={0})", username);
                    SearchResult adSearchResult = adSearch.FindOne();
                    return adSearchResult.GetDirectoryEntry();
                }
            }
        }
    }
}
