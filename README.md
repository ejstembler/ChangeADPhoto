ChangeADPhoto
=============

Allows changing of Active Directory photo.

### Getting Started

Before running the application, be sure to update the "Domain Controller" value in the [App.config](https://github.com/ejstembler/ChangeADPhoto/blob/master/App.config#L4) file.
